﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Newtonsoft.Json;

namespace MythicTable.GameSession
{
    public class SessionDelta
    {
        [JsonProperty("entities")]
        public IEnumerable<EntityOperation> Entities { get; set; }
    }

    public struct EntityOperation
    {
        [JsonProperty("id")]
        public string EntityId { get; set; }

        [JsonProperty("patch")]
        public JsonPatchDocument Patch { get; set; }

        public EntityOperation(string entityId, JsonPatchDocument patch)
        {
            this.EntityId = entityId;
            this.Patch = patch;
        }
    }
}
