﻿using System.Threading.Tasks;

namespace MythicTable.GameSession
{
    public interface ILiveClient
    {
        Task ConfirmDelta(SessionDelta delta);
    }
}
